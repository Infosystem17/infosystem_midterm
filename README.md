Summary: 
I found this game through inverntwithpython.com

It applies to Chapter 15 of the online book. The link is here: http://inventwithpython.com/chapter15.html .
This game is orthello. You can get more details at the link.


Remember the goal of the midterm is: "Provide screen shots of each member accessing the shared source on their computer and each member 
making a change to the code that is then reflected in the other member's code. Each member should write a 1 page paper 
listing the steps taken to complete the set up and how the sharing worked for them, after the page include the screen 
shots shared from yourself and your group mates. Use the group discussion board to form your groups. 
Each member must turn in an original document detailing their experiences with the software, only the screen shots should be the same."


What to take a screenshot of?

	When you go click source and click on the program, 
	there are three buttons on the right side. The furthest one says history. 
	This will show every time it was edited and who did it. Just make sure when you do edit, 
	you click on commit each time so it saves the new copy.

	We could send a screenshot of the history page. 
	Another option, all activity is marked on the commits page. 
	We could send a screenshot of that page as well. 

______________________________________________________________________________________________________
Everything shoudl be accessable and editable through bitbucket, however just in case I made a backup.


The backup version of the code is here:
https://drive.google.com/drive/folders/0B-IzfrqUam1EWjJwMHd3UDJyUE0?usp=sharing


________________________________________________________________________________________________________
Contact: 

Naylene Rondon  at  nrondon@vikings.sjrstate.edu or through the group discussion.

Dennis Dobbs at ddobbsjr@vikings.sjrstate.edu .

Keith Justice at  kjustice2@vikings.sjrstate.edu or at (863) 445-0997 .

Justin McKenzie at jmckenzie3@vikings.sjrstate.edu .
